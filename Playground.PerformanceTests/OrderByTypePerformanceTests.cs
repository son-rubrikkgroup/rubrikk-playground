using Playground.Enums;
using Playground.Tests.Performance;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Playground.PerformanceTests
{
    public class OrderByTypePerformanceTests: IClassFixture<MyFixture>
    {
        protected const int NumberOfMeasurement = 100000;
        protected static int NumberOfCall = 100000;

        [Fact]
        public void Call_OrderAsType_WithHybridType_Performance()
        {
            var expectedList = "1 < 2 < 11 < 22".Split("<").Select(x => x.Trim());
            var elements = new List<string> { "11", "22", "1", "2" };
            var testObj = new OrderByType(elements);

            PerformanceContext.Measure($"Call OrderAsType with Hybrid which {NumberOfMeasurement} times", () =>
            {
                testObj.OrderAsType(SortType.Hybrid);

            }, 100, NumberOfMeasurement);

            // asserts anything.
            Assert.Equal(NumberOfMeasurement, NumberOfCall);
        }

        [Fact]
        public void Call_OrderAsType_WithNumberType_Performance()
        {
            var expectedList = "1 < 2 < 11 < 22".Split("<").Select(x => x.Trim());
            var elements = new List<string> { "11", "22", "1", "2" };
            var testObj = new OrderByType(elements);

            PerformanceContext.Measure($"Call OrderAsType with Number which {NumberOfMeasurement} times", () =>
            {
                testObj.OrderAsType(SortType.Number);

            }, 100, NumberOfMeasurement);

            // asserts anything.
            Assert.Equal(NumberOfMeasurement, NumberOfCall);
        }

        [Fact]
        public void Call_OrderAsType_WithStringType_Performance()
        {
            var expectedList = "1 < 2 < 11 < 22".Split("<").Select(x => x.Trim());
            var elements = new List<string> { "11", "22", "1", "2" };
            var testObj = new OrderByType(elements);

            PerformanceContext.Measure($"Call OrderAsType with String which {NumberOfMeasurement} times", () =>
            {
                testObj.OrderAsType(SortType.String);

            }, 100, NumberOfMeasurement);

            // asserts anything.
            Assert.Equal(NumberOfMeasurement, NumberOfCall);
        }

        public void Dispose()
        {
            Console.WriteLine("Finish tests.");
        }
    }

    public class MyFixture : IDisposable
    {
        public MyFixture()
        {

        }

        public void Dispose()
        {
            Console.WriteLine("Finish tests.");
            PerformanceContext.StoreResult();
        }
    }
}
