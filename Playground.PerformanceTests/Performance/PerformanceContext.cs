﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Playground.Tests.Performance
{
    public class PerformanceContext
    {
        private static readonly Dictionary<string, TestResult> _measures = new Dictionary<string, TestResult>();

        public static int SuccessfulRuns { get; private set; }
        public static int FailedRuns { get; private set; }
        public static int ActionIterations { get; private set; }

        private static void EnsureUniqueName(string testName)
        {
            if (_measures.ContainsKey(testName))
            {
                throw new Exception("There is already a performance measure for: " + testName);
            }
        }

        private static void AddResult(string testName, TestResult result)
        {
            _measures[testName] = result;
        }

        //TODO: we can have another way to store the test result.
        public static void StoreResult(string resultsFolderPath = null)
        {
            if (_measures.Count == 0)
            {
                return;
            }

            if (string.IsNullOrEmpty(resultsFolderPath))
            {
                resultsFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PerformanceResults");
            }

            WriteToFile(resultsFolderPath, $"performanceResult{DateTime.UtcNow.ToString("yyyyMMddhhmmss")}.json", JsonConvert.SerializeObject(_measures));
        }

        private static string WriteToFile(string directoryPath, string fileName, string contents)
        {
            //Check whether directory exist or not if not then create it
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            string filePath = directoryPath + "\\" + fileName;
            File.WriteAllText(filePath, contents);

            return filePath;
        }

        public static TestResult Measure(string testName, Action action, int testIterations = 100, int actionIterations = 1)
        {
            EnsureUniqueName(testName);

            SuccessfulRuns = 0;
            FailedRuns = 0;
            ActionIterations = actionIterations;

            var watch = new Stopwatch();

            watch.Start();

            var errors = new List<Exception>();

            for (int i = 0; i < testIterations; i++)
            {
                for (int j = 0; j < actionIterations; j++)
                {
                    try
                    {
                        action();
                        SuccessfulRuns++;
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception);
                        errors.Add(exception);
                        FailedRuns++;
                    }
                }
            }

            watch.Stop();

            var averageDuration = (watch.ElapsedMilliseconds / testIterations);
            Console.WriteLine(
                $"The test '{testName}' ran {testIterations} time(s) successfully. Took: {watch.ElapsedMilliseconds} ms. Average: {averageDuration} per run");

            var result = new TestResult
            {
                Duration = averageDuration,
                Errors = errors
            };

            AddResult(testName, result);
            return result;
        }

        private static string GetErrorMessage(IEnumerable<Exception> exceptions)
        {
            if (!exceptions.Any())
            {
                return "0";
            }

            return string.Join(",", exceptions.Select(e => e.Message));
        }
    }

    [Serializable]
    public class TestResult
    {
        public long Duration { get; set; }
        public IEnumerable<Exception> Errors { get; set; }

    }
}
