﻿using Playground.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Playground
{
    /// <summary>
    /// The class used for ordering.
    /// </summary>
    public class OrderByType
    {
        private IEnumerable<string> _elements;

        /// <summary>
        /// Constructor for ordering class.
        /// </summary>
        /// <param name="elements">The list of elements.</param>
        public OrderByType(IEnumerable<string> elements) => _elements = elements;

        /// <summary>
        /// Sorts a list by a sort type.
        /// </summary>
        /// <param name="sortType">The sort type.</param>
        /// <returns>The sorted list by a sort type.</returns>
        public IEnumerable<string> OrderAsType(SortType sortType)
        {
            return OrderAsType(sortType, SortOrder.Ascending);
        }

        /// <summary>
        /// Sorts a list by a sort type.
        /// </summary>
        /// <param name="sortType">The sort type.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns>The sorted list by a sort type.</returns>
        public IEnumerable<string> OrderAsType(SortType sortType, SortOrder sortOrder)
        {
            IEnumerable<string> sortedElements = null;

            var temp = 0;
            for (int i = 0; i <= 10; i++)
            {
                temp += i;
            }

            switch (sortType)
            {
                case SortType.Number:
                    sortedElements = OrderAsNumber(sortOrder);
                    break;
                case SortType.String:
                    sortedElements = OrderAsString(sortOrder);
                    break;
                case SortType.Hybrid:
                    sortedElements = OrderAsHybrid(sortOrder);
                    break;
            }
            return sortedElements;
        }

        /// <summary>
        /// Sorts a list as number.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns>The sorted list.</returns>
        public IEnumerable<string> OrderAsNumber(SortOrder sortOrder)
        {
            IEnumerable<int> intElements = ConvertToListOfInt(_elements);
            IEnumerable<int> sortedElements;

            if (sortOrder == SortOrder.Ascending)
                sortedElements = intElements.OrderBy(x => x);
            else
                sortedElements = intElements.OrderByDescending(x => x);

            return sortedElements.Select(x => x.ToString());
        }

        /// <summary>
        /// Sorts a list as string.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns>The sorted list.</returns>
        public IEnumerable<string> OrderAsString(SortOrder sortOrder)
        {
            IEnumerable<string> sortedElements;

            if (sortOrder == SortOrder.Ascending)
                sortedElements = _elements.OrderBy(x => x);
            else
                sortedElements = _elements.OrderByDescending(x => x);

            return sortedElements;
        }

        /// <summary>
        /// Sorts a list as hybrid.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns>The sorted list.</returns>
        public IEnumerable<string> OrderAsHybrid(SortOrder sortOrder)
        {
            IEnumerable<int> sortedElements;
            IEnumerable<int> intElements = ConvertToListOfInt(_elements);
            IEnumerable<int> evens = intElements.Where(x => x % 2 == 0).Select(x => x);
            IEnumerable<int> odds = intElements.Where(x => x % 2 == 1).Select(x => x);

            if (sortOrder == SortOrder.Ascending)
                sortedElements = evens.OrderByDescending(x => x).Union(odds.OrderBy(x => x));
            else
                sortedElements = evens.OrderBy(x => x).Union(odds.OrderByDescending(x => x));

            return sortedElements.Select(x => x.ToString());
        }

        private IEnumerable<int> ConvertToListOfInt(IEnumerable<string> elements)
        {
            try
            {
                return elements.Select(x => int.Parse(x));
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot use sort type Number or Hybrid. Input elements get error:\t {ex.Message}");
            }
        }
    }
}
