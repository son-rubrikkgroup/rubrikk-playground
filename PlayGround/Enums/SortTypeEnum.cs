namespace Playground.Enums
{
    public enum SortType
    {
        Number,
        String,
        Hybrid
    }
}