
namespace Playground.Enums
{
    public enum SortOrder
    {
        Ascending,
        Descending
    }
}