﻿using Playground.Enums;
using Playground;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                DisplayUsage();
                return;
            }

            var filePath = @"C:\textfile.txt";
            var sortType = SortType.Number;
            var sortOrder = SortOrder.Ascending;

            try
            {
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i].ToLower())
                    {
                        case "-filepath":
                            filePath = args[i + 1].ToLower();
                            break;
                        case "-sortorder":
                            sortOrder = (SortOrder)Enum.Parse(typeof(SortOrder), args[i + 1]);
                            break;
                        case "-sorttype":
                            sortType = (SortType)Enum.Parse(typeof(SortType), args[i + 1]);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine($"The command parameters is not correct.\n");
                DisplayUsage();
                return;
            }

            Console.WriteLine($"The file path:");
            Console.WriteLine($"The file path:\t {filePath}");
            Console.WriteLine($"The sort order:\t {sortOrder}");
            Console.WriteLine($"The sort type:\t {sortType}");

            Process(filePath, sortType, sortOrder);

            Console.ReadLine();
        }

        private static void Process(string filePath, SortType sortType, SortOrder sortOrder)
        {
            var fileExists = System.IO.File.Exists(filePath);
            if (!fileExists)
            {
                throw new Exception("The file doesn't exist, file path might be incorrect.");
            }

            var fileContent = System.IO.File.ReadAllText(filePath);

            IEnumerable<string> elements = fileContent.Split(",").Select(x => x.Trim());
            var orderObj = new OrderByType(elements);
            IEnumerable<string> sortedElements = orderObj.OrderAsType(sortType, sortOrder);

            var comparer = sortOrder == SortOrder.Ascending ? " < " : " > ";
            Console.WriteLine($"Result:\t {String.Join(comparer, sortedElements.ToArray())}");
        }

        private static void DisplayUsage()
        {
            Console.WriteLine("\nUsage: Playground -filepath <FilePath> [-sortorder <SortOrder>] [-sorttype <SortType>] \n");
            Console.WriteLine("\t-filepath\tThe file path. Ex: \"C:\\textfile.txt\"");
            Console.WriteLine("\t-sortorder\tThe sort order: Ascending or Descending. The default value is Ascending.");
            Console.WriteLine("\t-sorttype\tThe sort type:");
            Console.WriteLine("\t\t\t. Number sort: 1 < 2 < 11 < 22");
            Console.WriteLine("\t\t\t. String sort: 1 < 11 < 2 < 22");
            Console.WriteLine("\t\t\t. Hybrid sort: 22 < 2 < 1 < 11 - even descending and then odd ascending.");
            Console.WriteLine("\t\t\tThe default value is Number.");
            Console.ReadLine();
        }
    }
}


// dotnet run -filepath "D:\Source\samplecode\Playground\testfile.txt"

