using Playground.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Playground.Tests
{
    public class OrderByTypeTests
    {
        [Theory]
        [InlineData(SortType.Number, "1 < 2 < 11 < 22")]
        [InlineData(SortType.String, "1 < 11 < 2 < 22")]
        [InlineData(SortType.Hybrid, "22 < 2 < 1 < 11")]
        //Note: verify some expectation only. We could do more test cases.
        public void CallOrderAsType(SortType sortType, string expected)
        {
            var expectedList = expected.Split("<").Select(x => x.Trim());
            var elements = new List<string> { "11", "22", "1", "2" };
            var testObj = new OrderByType(elements);

            var actual = testObj.OrderAsType(sortType);

            // assert
            Assert.Equal(expectedList, actual);
        }
    }
}
